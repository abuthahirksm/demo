import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { NgModule } from '@angular/core';
import { MenuPage } from '../menu/menu';

import { HomePage } from './home/home';
import { ListPage } from './list/list';
import { ChatPage } from './chat/chat.component';
@NgModule({
    declarations: [
        MenuPage,
        HomePage,
        ListPage,
        ChatPage
    ],
    imports: [
        IonicModule,
    ],
    bootstrap: [],
    entryComponents: [
        MenuPage,
        HomePage,
        ListPage,
        ChatPage
    ],
    providers: [
    ]
})
export class PageModule { }
