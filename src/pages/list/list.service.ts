import { Injectable } from '@angular/core';
import { Headers, Http,RequestOptions } from '@angular/http';
import 'rxjs/add/operator/toPromise';
//import $ from 'jquery';
import { env } from '../../env/env';

@Injectable()
export class ListService {
  private headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded', 'Accept': 'q=0.8;application/json;q=0.9' });
    private url = env.baseUrl+'/demo';
    constructor(private http: Http) { }

   private extractData(res: any) {
        let body = res.json();
        return body || [];
    }
    getCustomerList(query: any): Promise<any[]> {
        let params: URLSearchParams = new URLSearchParams();
        for (let key in query) {
            params.set(key.toString(), query[key]);
        }

        let options = new RequestOptions({
            headers: this.headers,
            search: params
        });

        const url = this.url+'/getCustomerList' ;
        return this.http.get(url, options)
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }

    private handleError(error: any): Promise<any> {
        //console.error('An error occurred', error); // for demo purposes only
        return Promise.reject(error.message || error);
    }

}