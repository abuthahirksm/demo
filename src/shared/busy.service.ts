import { Injectable } from '@angular/core';
import { LoadingController, Loading } from 'ionic-angular';

@Injectable()
export class BusyService{
    loading: Loading = null;

    constructor(private loadCtrl: LoadingController){ }

    presentBusy(text:string):void{
        let loadData = {
            content: text
        }

        this.showToast(loadData);
    }

    dismissBusy(): void{
        this.loading.dismiss();
    }

    private showToast(data:any):void{
        //this.loading ? this.loading.dismiss() : false;
        this.loading = this.loadCtrl.create(data);
        this.loading.present();
    }
}