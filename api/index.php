<?php
error_reporting(-1);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
ini_set('session.gc_maxlifetime', 28800);
ini_set("memory_limit", "2000M");
define('WP_MEMORY_LIMIT', '200M');
date_default_timezone_set('Asia/Kolkata');
ini_set('post_max_size', '500M');
//ini_set ('max_execution_time', 1200); 

try {
	// Initialize Composer autoloader
	if (!file_exists($autoload = __DIR__ . '/vendor/autoload.php')) {
		throw new \Exception('Composer dependencies not installed. Run `make install --directory app/api`');
	}
	require_once $autoload;

	// Initialize Slim Framework
	if (!class_exists('\\Slim\\Slim')) {
		throw new \Exception(
			'Missing Slim from Composer dependencies.'
			. ' Ensure slim/slim is in composer.json and run `make update --directory app/api`'
		);
	}

	// Run application
	$app = new \Api\Application();
	$app->run();

} catch (\Exception $e) {
	if (isset($app)) {
		$app->handleException($e);
	} else {
		http_response_code(500);
		header('Content-Type: application/json');
		echo json_encode(array(
			'status' => 500,
			'statusText' => 'Internal Server Error',
			'description' => $e->getMessage(),
		));
	}
}
