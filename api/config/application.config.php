<?php
return array(
    'env'       => 'dev',
    'imagepath' => 'http://demo.isquarebs.com/hills/attachments/',
    
    'fnyrstart' => 2014,
    'jms_companyid' => 17,
    'dev'		=> array(
        'photo_upload_path' => '../attachments/',
        'logFolder'     => '../logs',
        'tempFolder'    => '../temp'
    ),
    'prod'		=> array(
        'photo_upload_path' => '../attachments/',
        'logFolder'     => '../logs',
        'tempFolder'    => '../temp'
    )
);
?>
